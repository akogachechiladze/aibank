package com.example.aibankv10.ui.others

data class User( var uid: String? = "",
                 val email: String? = null,
                 val password:String? = null,
                 val username: String? = null,
                 val phoneNumber: String? = null,
                 val adult: Boolean? = null
)
