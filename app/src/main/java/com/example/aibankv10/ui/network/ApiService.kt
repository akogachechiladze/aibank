package com.example.assignment11repositorypattern.network


import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query


interface ApiService {

    @GET("")
    suspend fun requestSMS(@Query("sender")sender: String, @Query("message")message: String, @Query("number")phoneNumber: String,@Header("x-rapidapi-host")xrapidapihost: String,@Header("x-rapidapi-key")apikey: String)

}