package com.example.aibankv10.ui.oneTimeCodeFragment

import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class OneTimeCodeViewModel@Inject constructor() : ViewModel() {

}